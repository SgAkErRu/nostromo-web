/*
    SPDX-FileCopyrightText: 2023 Sergey Katunin <sulmpx60@yandex.ru>

    SPDX-License-Identifier: BSD-2-Clause
*/

export const NumericConstants = {
    EMPTY_LENGTH: 0,
    ZERO_IDX: 0,
    ZERO_TAB_IDX: 0,
    MOUSE_EVENT_NONE_BTN: 0,
    NEGATIVE_TAB_IDX: -1,
    NOT_FOUND_IDX: -1,
    IDX_STEP: 1,
    FILE_SIZE_PRECISION: 3,
} as const;
